--[[=============================================================================
    Commands received from the blind proxy (ReceivedFromProxy)

    Copyright 2015 Control4 Corporation. All Rights Reserved.
===============================================================================]]

-- This macro is utilized to identify the version string of the driver template version used.
if (TEMPLATE_VERSION ~= nil) then
	TEMPLATE_VERSION.proxy_commands = "2015.11.30"
end

--[[=============================================================================
    STOP()

    Description
    Stop the blinds

    Parameters
    none

    Notifies
    When a driver stops the blinds it should use he STOPPED notify to tell the proxy and UI's that the blind has stopped and where at if known.
===============================================================================]]
function STOP(bindingID)
    LogTrace("STOP")

    -- TODO:
    -- build the command using the function parameters and send it to the device here
    --local command = ""

    --PackAndQueueCommand("STOP", command)
    --MMB
    SendLevelControlClusterCommand(ZCL_STOP_COMMAND_ID, nil)
end

--[[=============================================================================
    LEVEL_TOGGLE()

    Description
    Toggle the blinds

    Parameters
    none

    Notifies
    When a driver toggles the blinds it should use he ??? notify to tell the proxy and UI's that the blind has ???.
===============================================================================]]
function LEVEL_TOGGLE(bindingID)
    LogTrace("LEVEL_TOGGLE")
    
    -- TODO:
    -- build the command using the function parameters and send it to the device here
    --local command = ""

    --PackAndQueueCommand("LEVEL_TOGGLE", command)
end

--[[=============================================================================
    SET_LEVEL_TARGET(level_target)

    Description
    Incoming command request for a blind level to be changed

    Parameters
    level_target - integer value of the target level the blinds should go to
    
    Notifies
    When a driver begins to move the hardware it should use the MOVING notify to tell the proxy and UI's that the blind is moving and where it's expected to stop at.
===============================================================================]]
function SET_LEVEL_TARGET(bindingID, level_target)
    

    -- MMB
    -- move to level with 0x000 transition time
    -- due to the weird behaviour with level_closed_secondary, we need to set level_closed_secondary as 255 instead of 254
    -- if we set level_closed_secondary to 254, it will consider as closed in the Control4 home screen, hence 255.
    -- Since the max currentLevel value is 254, we will limit the level to 254
    if (level_target > C4_LEVEL_OPEN) then
	   level_target = C4_LEVEL_OPEN
    elseif (level_target < C4_LEVEL_CLOSE) then
	   level_target = C4_LEVEL_CLOSE
    end
    LogTrace("SET_LEVEL_TARGET, level_target = " .. level_target)
    zcl_level_target = convertC4LeveltoZCLLevel(level_target)

    if (swBuildIDVersion < 1093) then
	   SendLevelControlClusterCommand(ZCL_MOVE_TO_LEVEL_WITH_ONOFF_COMMAND_ID, string.pack("b", zcl_level_target) .. string.pack("b", 0) .. string.pack("b", 0))
    else
	   SendCustomLevelControlClusterCommand(ZCL_CUSTOM_MOVE_TO_LEVEL_WITH_ONOFF_COMMAND_ID, string.pack("b", zcl_level_target))
    end
    --C4:SetVariable("TEMP_LEVEL", level_target)
    --DEV_MSG.Moving(level_target)
    
end

--=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=
-- Initialization function called from 
-- OnNetworkStatusChanged() / OnSerialConnectionChanged()
--=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=
function initialize()

    -- TODO:
    -- send message to device to get current level...
    
end

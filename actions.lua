--[[=============================================================================
    Lua Action Code

    Copyright 2015 Control4 Corporation. All Rights Reserved.
===============================================================================]]

-- This macro is utilized to identify the version string of the driver template version used.
if (TEMPLATE_VERSION ~= nil) then
	TEMPLATE_VERSION.actions = "2015.11.30"
end

-- TODO: Create a function for each action defined in the driver

function LUA_ACTION.TemplateVersion()
	TemplateVersion()
end

-- MMB
function LUA_ACTION.BlindOpen()
    LogTrace("BlindOpen()")
    --move to level command with 0x0000 transition time
    if (swBuildIDVersion < 1093) then
	   SendLevelControlClusterCommand(ZCL_MOVE_TO_LEVEL_WITH_ONOFF_COMMAND_ID, string.pack("b", ZCL_LEVEL_OPEN) .. string.pack("b", 0) .. string.pack("b", 0))
    else
	   SendCustomLevelControlClusterCommand(ZCL_CUSTOM_MOVE_TO_LEVEL_WITH_ONOFF_COMMAND_ID, string.pack("b", ZCL_LEVEL_OPEN))
    end
    --C4:SetVariable("TEMP_LEVEL", C4_LEVEL_OPEN)
end

function LUA_ACTION.BlindClose()
    LogTrace("BlindClose()")
    if (swBuildIDVersion < 1093) then
	   SendLevelControlClusterCommand(ZCL_MOVE_TO_LEVEL_WITH_ONOFF_COMMAND_ID, string.pack("b", ZCL_LEVEL_CLOSE) .. string.pack("b", 0) .. string.pack("b", 0))
    else
	   SendCustomLevelControlClusterCommand(ZCL_CUSTOM_MOVE_TO_LEVEL_WITH_ONOFF_COMMAND_ID, string.pack("b", ZCL_LEVEL_CLOSE))
    end
    --C4:SetVariable("TEMP_LEVEL", C4_LEVEL_CLOSE)
end

function LUA_ACTION.BlindStop()
    LogTrace("BlindStop()")
    SendLevelControlClusterCommand(ZCL_STOP_COMMAND_ID, nil)
end